﻿using Lab1;
using Lab1.Models;

var rp = new ResultsProcessing();
rp.ReadFromFile(@$"..\..\..\Data.txt");
rp.PrintList();

int number = 0;
while (true)
{
    Console.WriteLine("Выберите действие:\n" +
        "1 - Вывести все дни, в которые дул ветер в одном из направлений\n" +
        "2 - Вывести все дни, в которые дул ветер больше определенной скорости\n" +
        "3 - Сортировать список шейкерной сортировкой\n" +
        "4 - Сортировать список сортировкой слиянием\n" +
        "5 - Средняя скорость ветра в указанном месяце\n" +
        "0 - Выход");
    while (true)
    {
        try
        {
            number = Convert.ToInt32(Console.ReadLine());
            break;
        }
        catch
        {
            Console.WriteLine("Введите цифру от 1 до 5");
        }
    }

    if (number == 0)
    {
        break;
    }

    switch (number)
    {
        case 1:
            Console.WriteLine("Выберите и введите направление ветра:\n");
            foreach (var item in Enum.GetValues(typeof(WindDirection)))
            {
                Console.Write($"{item} | ");
            }

            while (true)
            {
                Console.WriteLine();
                string direction = Console.ReadLine();
                try
                {
                    rp.PrintResultsBySpecificWindDirection(direction);
                    break;
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            break;
        case 2:
            Console.WriteLine("Введите скорость ветра:");
            while (true)
            {
                try
                {
                    double speed = double.Parse(Console.ReadLine().Replace('.', ','));
                    rp.PrintResultsBySpecificSpeed(speed);
                    break;
                }
                catch
                {
                    Console.WriteLine("Проверьте введенное значение");
                }
            }

            break;
        case 3:
            Console.WriteLine("Выберите параметр сортировки:\n1 - По убыванию скорости ветра\n2 - По возрастанию направления ветра, месяца, дня");
            while (true)
            {
                try
                {
                    number = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch
                {
                    Console.WriteLine("Проверьте введенное значение");
                }
            }

            switch (number)
            {
                case 1:
                    rp.PrintList(rp.SortListWithShakerSort(SortingParameter.SortBySpeed));
                    break;
                case 2:
                    rp.PrintList(rp.SortListWithShakerSort(SortingParameter.SortByDirection));
                    break;
                default:
                    Console.WriteLine("Проверьте введенное значение");
                    break;
            }

            break;
        case 4:
            Console.WriteLine("Выберите параметр сортировки:\n1 - По убыванию скорости ветра\n2 - По возрастанию направления ветра, месяца, дня");
            while (true)
            {
                try
                {
                    number = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch
                {
                    Console.WriteLine("Проверьте введенное значение");
                }
            }

            switch (number)
            {
                case 1:
                    rp.PrintList(rp.SortListWithMergeSort(SortingParameter.SortBySpeed));
                    break;
                case 2:
                    rp.PrintList(rp.SortListWithMergeSort(SortingParameter.SortByDirection));
                    break;
                default:
                    Console.WriteLine("Проверьте введенное значение");
                    break;
            }

            break;
        case 5:
            while (true)
            {
                Console.WriteLine("Введите номер месяца (1 - 12):");
                try
                {
                    number = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine($"Средняя скорость ветра в {number} месяце: {Math.Round(rp.GetAverageWindSpeedByMonth(number), 3)} м/с");
                    break;
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch
                {
                    Console.WriteLine("Проверьте введенное значение");
                }
            }

            break;
        default:
            Console.WriteLine("Попробуйте снова");
            break;
    }
}