﻿namespace Lab1.Models
{
    public enum SortingParameter
    {
        SortBySpeed,
        SortByDirection
    }
}
