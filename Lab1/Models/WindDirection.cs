﻿namespace Lab1.Models
{
    public enum WindDirection
    {
        North, 
        NorthEast, 
        East, 
        SouthEast, 
        South, 
        SouthWest, 
        West, 
        NorthWest
    }
}
