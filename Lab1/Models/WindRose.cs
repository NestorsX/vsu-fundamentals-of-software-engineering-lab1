﻿namespace Lab1.Models
{
    public readonly struct WindRose
    {
        private readonly int _day;
        private readonly int _month;
        private readonly WindDirection _direction;
        private readonly double _windSpeed;

        public int Day { get { return _day; } }
        public int Month { get { return _month; } }
        public WindDirection Direction { get { return _direction; } }
        public double WindSpeed { get { return _windSpeed; } }

        public WindRose(int day, int month, WindDirection direction, double windSpeed)
        {
            _day = day;
            _month = month;
            _direction = direction;
            _windSpeed = windSpeed;
        }
    }
}