﻿using Lab1.Models;

namespace Lab1
{
    public class ResultsProcessing
    {
        private readonly List<WindRose> _windList;
        private int sortStage = 0;

        public List<WindRose> WindList { get { return _windList; } }

        public ResultsProcessing()
        {
            _windList = new List<WindRose>();
        }

        /// <summary>
        /// Чтение данных из файла по указанному пути
        /// </summary>
        /// <param name="filename"></param>
        public void ReadFromFile(string filename)
        {
            using (var streamReader = new StreamReader(filename))
            {
                while (!streamReader.EndOfStream)
                {
                    string? line = streamReader.ReadLine();
                    string[] record = line.Split(" ");
                    _windList.Add(new WindRose(
                        Convert.ToInt32(record[0]),
                        Convert.ToInt32(record[1]),
                        (WindDirection)Enum.Parse(typeof(WindDirection), record[2], true),
                        Double.Parse(record[3].Replace('.', ','))
                    ));
                }
            }
        }

        /// <summary>
        /// Подсчет средней скорости ветра в указанном месяце
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public double GetAverageWindSpeedByMonth(int month)
        {
            var windListByMonth = _windList.Where(x => x.Month == month);
            if (windListByMonth.Count() == 0)
            {
                throw new ArgumentException($"Список не содержит {month} месяца");
            }

            return windListByMonth.Average(x => x.WindSpeed);
        }

        /// <summary>
        /// Вывод в консоль одного элемента списка розы ветров
        /// </summary>
        /// <param name="rose"></param>
        private static void PrintConcreteWindRose(WindRose rose)
        {
            Console.WriteLine($"{rose.Day,5}{rose.Month,7}{rose.Direction,19}{rose.WindSpeed,22}");
        }

        /// <summary>
        /// Вывод в консоль переданного списка розы ветров
        /// </summary>
        /// <param name="roses"></param>
        public void PrintList(IEnumerable<WindRose> roses)
        {
            Console.WriteLine($"{"День",5}{"Месяц",7}{"Направление ветра",19}{"Скорость ветра (м/с)",22}");
            foreach (var rose in roses)
            {
                PrintConcreteWindRose(rose);
            }

            Console.WriteLine();
        }

        /// <summary>
        /// Вывод всей розы ветров
        /// </summary>
        public void PrintList()
        {
            PrintList(_windList);
        }

        /// <summary>
        /// Вывод розы ветров по указанному направлению ветра
        /// </summary>
        /// <param name="windDirection"></param>
        /// <exception cref="ArgumentException"></exception>
        public void PrintResultsBySpecificWindDirection(string windDirection)
        {
            if (!_windList.Any(x => x.Direction.ToString().Equals(windDirection)))
            {
                throw new ArgumentException("Такого направления ветра нет в списке");
            }

            Console.WriteLine($"Результаты по направлению \"{windDirection}\"");
            PrintList(_windList.Where(x => x.Direction.ToString().Equals(windDirection)));
        }

        /// <summary>
        /// Вывод розы ветров в дни, когда скорость ветра больше указанной
        /// </summary>
        /// <param name="windSpeed"></param>
        public void PrintResultsBySpecificSpeed(double windSpeed)
        {
            Console.WriteLine($"Результаты со скоростью ветра больше, чем {windSpeed}");
            PrintList(_windList.Where(x => x.WindSpeed > windSpeed));
        }

        /// <summary>
        /// Шейкерная сортировка с зависимостью от передаваемого параметра
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public IEnumerable<WindRose> SortListWithShakerSort(SortingParameter parameter)
        {
            var sortedList = new List<WindRose>(_windList);
            int left;
            int right;
            switch (parameter)
            {
                case SortingParameter.SortBySpeed:
                    left = 0;
                    right = sortedList.Count - 1;
                    while (left <= right)
                    {
                        for (var i = right; i > left; --i)
                        {
                            if (sortedList[i - 1].WindSpeed < sortedList[i].WindSpeed)
                            {
                                (sortedList[i - 1], sortedList[i]) = (sortedList[i], sortedList[i - 1]);
                            }
                        }

                        ++left;
                        for (int i = left; i < right; ++i)
                        {
                            if (sortedList[i].WindSpeed < sortedList[i + 1].WindSpeed)
                            {
                                (sortedList[i], sortedList[i + 1]) = (sortedList[i + 1], sortedList[i]);
                            }
                        }

                        --right;
                    }

                    break;
                case SortingParameter.SortByDirection:
                    left = 0;
                    right = sortedList.Count - 1;
                    while (left <= right)
                    {
                        for (var i = right; i > left; --i)
                        {
                            if (sortedList[i - 1].Day > sortedList[i].Day)
                            {
                                (sortedList[i - 1], sortedList[i]) = (sortedList[i], sortedList[i - 1]);
                            }
                        }

                        ++left;
                        for (int i = left; i < right; ++i)
                        {
                            if (sortedList[i].Day > sortedList[i + 1].Day)
                            {
                                (sortedList[i], sortedList[i + 1]) = (sortedList[i + 1], sortedList[i]);
                            }
                        }

                        --right;
                    }

                    left = 0;
                    right = sortedList.Count - 1;
                    while (left <= right)
                    {
                        for (var i = right; i > left; --i)
                        {
                            if (sortedList[i - 1].Month > sortedList[i].Month)
                            {
                                (sortedList[i - 1], sortedList[i]) = (sortedList[i], sortedList[i - 1]);
                            }
                        }

                        ++left;
                        for (int i = left; i < right; ++i)
                        {
                            if (sortedList[i].Month > sortedList[i + 1].Month)
                            {
                                (sortedList[i], sortedList[i + 1]) = (sortedList[i + 1], sortedList[i]);
                            }
                        }

                        --right;
                    }

                    left = 0;
                    right = sortedList.Count - 1;
                    while (left <= right)
                    {
                        for (var i = right; i > left; --i)
                        {
                            if (sortedList[i - 1].Direction > sortedList[i].Direction)
                            {
                                (sortedList[i - 1], sortedList[i]) = (sortedList[i], sortedList[i - 1]);
                            }
                        }

                        ++left;
                        for (int i = left; i < right; ++i)
                        {
                            if (sortedList[i].Direction > sortedList[i + 1].Direction)
                            {
                                (sortedList[i], sortedList[i + 1]) = (sortedList[i + 1], sortedList[i]);
                            }
                        }

                        --right;
                    }

                    break;
            }

            return sortedList;
        }

        /// <summary>
        /// Сортировка слиянием с зависимостью от передаваемого параметра
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public List<WindRose> SortListWithMergeSort(SortingParameter parameter)
        {
            switch (parameter)
            {
                case SortingParameter.SortBySpeed:
                    return SortListWithMergeSortByParameter(_windList, parameter);
                case SortingParameter.SortByDirection:
                    return SortListWithMergeSortByDirection(_windList, parameter);
                default:
                    throw new ArgumentException("Неверный параметр", nameof(parameter));
            }
        }

        /// <summary>
        /// Сортировка слиянием по направлению ветра, проходящая через три этапа
        /// </summary>
        /// <param name="unsorted"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private List<WindRose> SortListWithMergeSortByDirection(List<WindRose> unsorted, SortingParameter parameter)
        {
            sortStage = 0;
            var result = unsorted;
            for (var i = 0; i < 3; i++)
            {
                result = SortListWithMergeSortByParameter(result, parameter);
                sortStage++;
            }

            return result;
        }

        /// <summary>
        /// Отправная точка сортировки слиянием
        /// </summary>
        /// <param name="unsorted"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private List<WindRose> SortListWithMergeSortByParameter(List<WindRose> unsorted, SortingParameter parameter)
        {
            if (unsorted.Count <= 1)
            {
                return unsorted;
            }

            var left = new List<WindRose>();
            var right = new List<WindRose>();
            int middle = unsorted.Count / 2;
            for (int i = 0; i < middle; i++)
            {
                left.Add(unsorted[i]);
            }

            for (int i = middle; i < unsorted.Count; i++)
            {
                right.Add(unsorted[i]);
            }

            left = SortListWithMergeSortByParameter(left, parameter);
            right = SortListWithMergeSortByParameter(right, parameter);
            if (parameter == SortingParameter.SortByDirection)
            {
                switch (sortStage)
                {
                    case 0:
                        return MergeByDay(left, right);
                    case 1:
                        return MergeByMonth(left, right);
                    case 2:
                        return MergeByDirection(left, right);
                }
            }

            return MergeBySpeed(left, right);
        }

        /// <summary>
        /// Сортировка слиянием по убыванию скорости ветра
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        private static List<WindRose> MergeBySpeed(List<WindRose> left, List<WindRose> right)
        {
            var result = new List<WindRose>();
            while (left.Count > 0 || right.Count > 0)
            {
                if (left.Count > 0 && right.Count > 0)
                {
                    if (left.First().WindSpeed <= right.First().WindSpeed)
                    {
                        result.Add(right.First());
                        right.Remove(right.First());
                        continue;
                    }

                    result.Add(left.First());
                    left.Remove(left.First());
                    continue;
                }

                if (left.Count > 0)
                {
                    result.Add(left.First());
                    left.Remove(left.First());
                    continue;
                }

                result.Add(right.First());
                right.Remove(right.First());
            }

            return result;
        }

        /// <summary>
        /// Первый этам сортировки по направлению ветра: сортировка по дню
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        private static List<WindRose> MergeByDay(List<WindRose> left, List<WindRose> right)
        {
            var result = new List<WindRose>();
            while (left.Count > 0 || right.Count > 0)
            {
                if (left.Count > 0 && right.Count > 0)
                {
                    if (left.First().Day <= right.First().Day)
                    {
                        result.Add(left.First());
                        left.Remove(left.First());
                        continue;
                    }

                    result.Add(right.First());
                    right.Remove(right.First());
                    continue;
                }

                if (left.Count > 0)
                {
                    result.Add(left.First());
                    left.Remove(left.First());
                    continue;
                }

                result.Add(right.First());
                right.Remove(right.First());
            }

            return result;
        }

        /// <summary>
        /// Второй этам сортировки по направлению ветра: сортировка по месяцу
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        private static List<WindRose> MergeByMonth(List<WindRose> left, List<WindRose> right)
        {
            var result = new List<WindRose>();
            while (left.Count > 0 || right.Count > 0)
            {
                if (left.Count > 0 && right.Count > 0)
                {
                    if (left.First().Month <= right.First().Month)
                    {
                        result.Add(left.First());
                        left.Remove(left.First());
                        continue;
                    }

                    result.Add(right.First());
                    right.Remove(right.First());
                    continue;
                }

                if (left.Count > 0)
                {
                    result.Add(left.First());
                    left.Remove(left.First());
                    continue;
                }

                result.Add(right.First());
                right.Remove(right.First());
            }

            return result;
        }

        /// <summary>
        /// Третий этам сортировки по направлению ветра: сортировка по направлению ветра
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        private static List<WindRose> MergeByDirection(List<WindRose> left, List<WindRose> right)
        {
            var result = new List<WindRose>();
            while (left.Count > 0 || right.Count > 0)
            {
                if (left.Count > 0 && right.Count > 0)
                {
                    if (left.First().Direction <= right.First().Direction)
                    {
                        result.Add(left.First());
                        left.Remove(left.First());
                        continue;
                    }

                    result.Add(right.First());
                    right.Remove(right.First());
                    continue;
                }

                if (left.Count > 0)
                {
                    result.Add(left.First());
                    left.Remove(left.First());
                    continue;
                }

                result.Add(right.First());
                right.Remove(right.First());
            }

            return result;
        }
    }
}